'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.createTable('addresses', {
     id: {
       type: Sequelize.UUID,
       primaryKey: true,
       allowNull: false,
       defaultValue: Sequelize.literal('uuid_generate_v4()')
     },
     zipcode: {
       type: Sequelize.STRING,
       allowNull: false,
     },
     street: {
       type: Sequelize.STRING,
       allowNull: false,
     },
     number: {
       type: Sequelize.INTEGER,
       allowNull: false,
     },
     user_id: {
      type: Sequelize.UUID,
      allowNull: false,
      references: { model: 'users', key: 'id'},
      onUpdate: 'CASCADE',
      onDelete: 'CASCADE'
     },
     created_at: {
      type: Sequelize.DATE,
      allowNull: false,
    },
    updated_at: {
      type: Sequelize.DATE,
      allowNull: false,
    }
    })
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.dropTable('addresses');
  }
};
