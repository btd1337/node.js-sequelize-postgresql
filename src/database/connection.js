const Sequelize = require('sequelize');
const dbConfig = require('../config/database');
const Address = require('./../models/address.model');
const Tech = require('./../models/tech.model');
const User = require('./../models/user.model');

const connection = new Sequelize(dbConfig);

User.init(connection);
Address.init(connection);
Tech.init(connection);

User.associate(connection.models);
Address.associate(connection.models);
Tech.associate(connection.models);

module.exports = connection;