module.exports = {
    dialect: 'postgres',
    host: 'localhost',
    database: 'sql_node_db',
    username: 'docker',
    password: 'docker',
    define: {
        timestamps: true,
        underscored: true,
    }
}