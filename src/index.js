require('dotenv').config();

const express = require('express');
const routes = require('./routes');
const bodyParser = require('body-parser');

require('./database/connection');

const app = express();

app.use(express.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(routes);
app.listen(3000);