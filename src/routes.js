const express = require('express');
const AddressesController = require('./controllers/addresses.controller');
const ReportsController = require('./controllers/reports.controller');
const TechsController = require('./controllers/techs.controller');
const UsersController = require('./controllers/users.controller');

const routes = express.Router();

routes.get('/', (req, res) => {
    return res.json({message: 'Hello World!'});
});

// USERS
routes.post('/users', (req, res) => UsersController.createOne(req, res));
routes.get('/users', (req, res) => UsersController.findMany(req, res));

routes.post('/users/:id/addresses', (req, res) => AddressesController.createOne(req, res));
routes.get('/users/:id/addresses', (req, res) => AddressesController.findMany(req, res));

routes.post('/users/:id/techs', (req, res) => TechsController.createOne(req, res));
routes.get('/users/:id/techs', (req, res) => TechsController.findMany(req, res));
routes.delete('/users/:id/techs', (req, res) => TechsController.removeFromUser(req, res));

// REPORTS
routes.get('/reports', (req, res) => ReportsController.getMany(req, res));

module.exports = routes;