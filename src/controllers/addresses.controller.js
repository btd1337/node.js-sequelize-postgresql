const User = require('../models/user.model');
const Address = require('../models/address.model');
const { findMany } = require('./users.controller');

module.exports = {
    async createOne(req, res) {
        const userId = req.params.id;
        const { zipcode, street, number } = req.body;

        const user = await User.findByPk(userId);

        if (!user) {
            return res.status(400).json({ error: 'User not found'});
        }

        const address = await Address.create({
            zipcode,
            street,
            number,
            "user_id": userId
        })

        return res.json(address);
    },

    async findMany(req, res) {
        const userId = req.params.id;

        const user = await User.findOne({id: userId, include: { association: 'addresses' }});

        if (!user) {
            return res.status(400).json({ error: 'User not found'});
        }

        return res.json(user.addresses);
    }
}