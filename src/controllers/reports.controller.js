const { Op } = require('sequelize');
const User = require('../models/user.model');

module.exports = {
    async getMany(req, res) {
        const users = await User.findAll({
            attributes: ['name', 'email'],
            where: {
                email: {
                    [Op.like]: '%@hackety.com.br',
                }
            },
            include: [
                { association: 'addresses', where: { street: 'Eurico Viana'} },
                { association: 'techs',
                    where: { 
                        name: {
                            [Op.like]: 'Angular%',
                        }
                }, required: false
            }
            ]
        })
        return res.json(users);
    }

}