const Tech = require('../models/tech.model');
const User = require('../models/user.model');

module.exports = {
    async createOne(req, res) {
        const userId = req.params.id;
        const { name } = req.body;

        const user = await User.findByPk(userId);

        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }

        const [ tech ] = await Tech.findOrCreate({
            where: { name }
        })

        await user.addTech(tech);

        return res.json(tech);
    },

    async findMany(req, res) {
        const userId = req.params.id;

        const user = await User.findByPk(userId, {
            include: { association: 'techs', through: { attributes: [] }}
        });

        if (!user) {
            return res.status(404).json({ error: 'User not found'});
        }

        return res.json(user.techs);
    },

    async removeFromUser(req, res) {
        const userId = req.params.id;
        const { name } = req.body;

        const user = await User.findByPk(userId);

        console.log('user', user);

        if (!user) {
            return res.status(404).json({ error: 'User not found' });
        }

        const tech = await Tech.findOne({ where: { name } });

        if (!tech) {
            return res.status(404).json({ error: 'Tech not found'});
        }

        await user.removeTech(tech);

        return res.status(200).json();
    }
}