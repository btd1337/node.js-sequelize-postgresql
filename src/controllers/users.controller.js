const { findOne } = require("../models/user.model");
const User = require("../models/user.model");

module.exports = {
    async findMany(req, res) {
        const users = await User.findAll();

        return res.json(users);
    },

    async createOne(req, res) {
        const { name, email } = req.body;
        const user = await User.create({ name, email });

        return res.json(user);
    }
}